<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preparan".
 *
 * @property int $id
 * @property string $alumno
 * @property int $oposicion
 * @property string $fechaalta
 * @property string $fechabaja
 *
 * @property Pagos[] $pagos
 * @property Alumnos $alumno0
 * @property Oposiciones $oposicion0
 */
class Preparan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'preparan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['oposicion'], 'integer'],
            [['fechaalta', 'fechabaja'], 'safe'],
            [['alumno'], 'string', 'max' => 255],
            [['alumno', 'oposicion', 'fechaalta'], 'unique', 'targetAttribute' => ['alumno', 'oposicion', 'fechaalta']],
            [['alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['alumno' => 'nif']],
            [['oposicion'], 'exist', 'skipOnError' => true, 'targetClass' => Oposiciones::className(), 'targetAttribute' => ['oposicion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alumno' => 'Alumno',
            'oposicion' => 'Oposicion',
            'fechaalta' => 'Fechaalta',
            'fechabaja' => 'Fechabaja',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagos()
    {
        return $this->hasMany(Pagos::className(), ['preparan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumno0()
    {
        return $this->hasOne(Alumnos::className(), ['nif' => 'alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOposicion0()
    {
        return $this->hasOne(Oposiciones::className(), ['id' => 'oposicion']);
    }
}
