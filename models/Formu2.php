<?php

namespace app\models;

use \yii\base\Model;

class Formu2 extends Model{
    public $nombre;
    public $apellidos;
    public $telefono;
    public $correo;
    public $casado;
    private $valoresPoblaciones=["Santander"=>"Santander","Potes"=>"Potes","Laredo"=>"Laredo"];
    public $poblacion;
    
    
    public function rules(){
        return [
          ['nombre','required','message'=>'{attribute} es obligatorio'],
          ['nombre','string','max'=>20,'tooLong'=>'{attribute} debe tener como máximo 20 caracteres'],
          ['correo','required','message'=>'{attribute} es obligatorio'],
          ['correo','email','message'=>'{attribute} debe ser un email'],
          ['telefono','required','when'=>function($model){
            $model->correo=="";
          }],
          ['poblacion','in','range'=> array_keys($this->getValoresPoblaciones())],
          [['nombre','apellidos','telefono','correo','casado'],'safe'],
            
        ];
    }
    
    public function attributeLabels() {
        return [
          'nombre'=>"Nombre",
          'correo'=>"Correo electrónico",
          'poblacion'=>"Población residencia",  
          'telefono'=>'Teléfono',
          'poblacion'=>'Población',
          'casado'=>'Casado',  
        ];
    }


    public function getValoresPoblaciones(){
        return $this->valoresPoblaciones;
    }
};



