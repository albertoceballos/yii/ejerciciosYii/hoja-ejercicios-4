<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pagos".
 *
 * @property int $id
 * @property int $preparan
 * @property string $fechaPago
 * @property double $importe
 *
 * @property Preparan $preparan0
 */
class Pagos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pagos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['preparan'], 'integer'],
            [['fechaPago'], 'safe'],
            [['importe'], 'number'],
            [['preparan', 'fechaPago'], 'unique', 'targetAttribute' => ['preparan', 'fechaPago']],
            [['preparan'], 'exist', 'skipOnError' => true, 'targetClass' => Preparan::className(), 'targetAttribute' => ['preparan' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'preparan' => 'Preparan',
            'fechaPago' => 'Fecha Pago',
            'importe' => 'Importe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreparan0()
    {
        return $this->hasOne(Preparan::className(), ['id' => 'preparan']);
    }
}
