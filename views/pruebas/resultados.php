<div class="col-md-4 text-center">
<?php

use yii\widgets\DetailView;

echo DetailView::widget([
   'model'=>$model,
    'attributes'=>[
        [
        'label'=>$model->getAttributeLabel('Nombre'),
        'value'=>$model->nombre,
        ],   
    ],
]);

echo DetailView::widget([
   'model'=>$model,
    'attributes'=>[
        [
        'label'=>$model->getAttributeLabel('apellidos'),
        'value'=>$model->apellidos,
        ],
    ],    
]);

echo DetailView::widget([
   'model'=>$model,
    'attributes'=>[
        [
        'label'=>$model->getAttributeLabel('telefono'),
        'value'=>$model->telefono,
        ],
    ],    
]);

echo DetailView::widget([
   'model'=>$model,
    'attributes'=>[
        [
        'label'=>$model->getAttributeLabel('correo'),
        'value'=>$model->correo,
        ],
    ],    
]);

echo DetailView::widget([
   'model'=>$model,
    'attributes'=>[
        [
        'label'=>$model->getAttributeLabel('poblacion'),
        'value'=>$model->poblacion,
        ],
    ],    
]);
echo DetailView::widget([
   'model'=>$model,
    'attributes'=>[
        [
        'label'=>$model->getAttributeLabel('casado'),
        'value'=>$model->casado,
        ],
    ],    
]);

?>
</div>


