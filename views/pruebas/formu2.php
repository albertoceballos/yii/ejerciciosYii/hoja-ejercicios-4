<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form=ActiveForm::begin([
    'id'=>'login-form',
]);

  echo $form->field($model, 'nombre')->textInput();
  echo $form->field($model, 'apellidos')->textInput();
  echo $form->field($model, 'telefono')->textInput();
  echo $form->field($model, 'correo')->textInput();
  echo $form->field($model, 'casado')->checkbox(['value'=>'Si','uncheck' => 'No']);
  echo $form->field($model,'poblacion')->dropDownList($model->getValoresPoblaciones());
  echo Html::submitButton('Enviar');
  ActiveForm::end();
  
 ?>


