<?php

namespace app\controllers;

use yii\web\Controller;

class PruebasController extends Controller{
    public function actionIndex(){
        
        if(\Yii::$app->request->post()){
            $datos= \Yii::$app->request->post('nombre');
            return $this->render("index",["mensaje"=>$datos]);
        }else{
            return $this->render("index",["mensaje"=>"Probando un poco Yii2, nada Enviado"]);
        }
        
    }
    
    public function actionFormu1(){
        return $this->render("formu1");
    }
    
    public function actionFormu2(){
        $model=new \app\models\Formu2;
        
        if($model->load(\Yii::$app->request->post()) && $model->validate()){
            return $this->render('resultados',['model'=>$model]);
        }else{
            return $this->render('formu2',['model'=>$model]);
        }
    }
}

